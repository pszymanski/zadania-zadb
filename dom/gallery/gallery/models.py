# -*- coding:utf-8 -*-
from django.db import models
from PIL import Image
from cStringIO import StringIO
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files.storage import default_storage as storage
import os

class Directory(models.Model):
    path=models.TextField()
    parent=models.ForeignKey('self')
    
    def __init__(self, path="/"):
        pass

    def save(self, ):
        for dir in dirs:
            dir.save
        self.save()
    
class Pic(models.Model):
    #parent=models.ForeignKey(Directory)
    image = models.ImageField(upload_to ="photos/originals/%d/")
    thumbs=[]

    def generateThumbs(self, image):
        for size in (64,128,256):
            self.image.seek(0)
            thumb=Thumb()
            thumb.image=image
            thumb.pic=self
            thumb.create(size=size)
            self.thumbs.append(thumb)
        
class Thumb(models.Model):
    image=models.ImageField(upload_to="photos/thumbs/%d/")
    pic=models.ForeignKey(Pic, null=True, blank=True)
    def create(self, size):
        image=Image.open(self.pic.image)
        image.thumbnail((size,size),Image.ANTIALIAS)

        temp_handle = StringIO()
        image.save(temp_handle, 'png')
        temp_handle.seek(0)
        print(self.image.name)
        suf = SimpleUploadedFile(os.path.split(self.image.name)[-1], temp_handle.read(), content_type='image/png')
        self.image.save(suf.name+"_"+str(size)+'.png', suf)

