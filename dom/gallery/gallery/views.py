# -*- coding:utf-8 -*-
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from gallery.models import Pic,Thumb
from gallery.tasks import *

def index(request):
    clean_works()
    pics=Pic.objects.all()
    thumbs=[]
    for pic in pics:
        thumbsForPic=Thumb.objects.filter(pic=pic)
        thumbs.append(thumbsForPic)
    return render(request, 'index.html', {'pics':thumbs,'jobs':jobs})

def add(request):
    if request.method=='POST':
        pic=Pic()
        pic.image=request.FILES['picture']
        pic.save()
        pic.generateThumbs(image=request.FILES['picture'])
        for thumb in pic.thumbs:
            thumb.save()
        return redirect(reverse('index'))
    else:
        return render(request, 'add.html')

def detail(request):
    pic = Pic.objects.get(pk=request.GET['pic_id'])
    if pic is not None:
        return render(request, 'detail.html', {'pic': pic})
    else:
        return redirect(reverse('index'))

def task(request):
    #init_work(request)
    #job=do_work.delay()
    #poll_state(request,job.id)
    #return HttpResponse("asd")
    return render(request, 'echo.html',{'what':poll_state(request)})
