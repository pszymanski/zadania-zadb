# -*- coding:utf-8 -*-
from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'gallery.views.index', name='index'),
    url(r'add/','gallery.views.add', name='add'),
    url(r'detail/','gallery.views.detail', name='detail'),
    url(r'task/','gallery.views.task', name='task'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^photos/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT+"/photos/", 'show_indexes': True}),
    url(r'init_work/', 'gallery.tasks.init_work', name='init_work'),
    url(r'poll_state/', 'gallery.tasks.poll_state', name="poll_state"),
)
