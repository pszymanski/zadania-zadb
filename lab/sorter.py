#!/usr/bin/env python
import pika
import time
import czynniki

connection = pika.BlockingConnection(pika.ConnectionParameters(host='194.29.175.241'))
channel = connection.channel()

channel.queue_declare(queue='sorter', durable=True)

sortedList=[]

def on_request(ch, method, properties, body):
    global sortedList
    if body=='^':
        sortedList=[]
        ch.basic_ack(delivery_tag = method.delivery_tag)
    elif body=='$':
        sortedList.sort()
        response=""
        for number in sortedList:
            print number, czynniki.rozklad(number)
        ch.basic_ack(delivery_tag = method.delivery_tag)
    else:
        num=int(body)
        sortedList.append(num)
        ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(on_request, queue='sorter')

import randomorg

channel.start_consuming()
