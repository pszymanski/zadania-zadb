# -*- coding: utf-8 -*-
import memcache, random, time, timeit
from math import *
mc = memcache.Client(['194.29.175.241:11211','194.29.175.242:11211'])

def rozklad(x):
    cached_value=mc.get(str(x))
    if cached_value is not None:
        return cached_value
    if x<=0:
        return 0
    i=2
    e=floor(sqrt(x))
    r=[] #używana jest tablica (lista), nie bepośrednie wypisywanie
    while i<=e:
        if x%i==0:
            r.append(i)
            x/=i
            e=floor(sqrt(x))
        else:
            i+=1
    if x>1: r.append(x)
    mc.set(str(x),r)
    return r
