# -*- coding: utf-8 -*-
import pika, requests

question={'num':200,'min':4000,'max':5000,'col':1,'base':10,'format':'plain','rnd':'new'}
r=requests.get('http://www.random.org/integers/',params=question)

connection =pika.BlockingConnection(pika.ConnectionParameters(host='194.29.175.241'))
channel = connection.channel()

channel.queue_declare(queue='randomorg', durable=True)

for number in ("^\n"+r.text+"$").split('\n'):
    channel.basic_publish(exchange='', routing_key='sorter', body=number, properties=pika.BasicProperties(delivery_mode = 2,))
